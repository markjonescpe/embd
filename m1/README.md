Mark Jones - 5 July 2018 -  Assignment #1 (C1M1) for introduction to embedded systems Coursera online course.

This assignment is meant to have the programmer exercize his/her C programming chops to be ready for later assignments.
The student is required to write a program that:

 Performs basic functions on arrays of unsigned chars (0 - 255.)  Including:
		Printing stats, printing the array, returning array median,
 		returning array mean, returning array max, returning array min,
 		and sorting the array in place in descending order.
