/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c
 * @brief Perorms basic statistical functions on an array of unsigned chars
 *
 * Performs basic functions on arrays of unsigned chars (0 - 255.)  Including:
 *		Printing stats, printing the array, returning array median,
 * 		returning array mean, returning array max, returning array min,
 * 		and sorting the array in place in descending order.
 *
 * @author Mark Jones
 * @date July 5th, 2018
 *
 */



#include <stdio.h>
#include <stdlib.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)
#define PRINT_ROW_SIZE (5)


/*private module functions*/
int sort_uchar_descending(const void* ch1, const void* ch2);

void main() {

	unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
		                    114, 88,   45,  76, 123,  87,  25,  23,
		                    200, 122, 150, 90,   92,  87, 177, 244,
		                    201,   6,  12,  60,   8,   2,   5,  67,
		                      7,  87, 250, 230,  99,   3, 100, 90};

	printf("Unsorted provided array: \n");
	print_array(test, SIZE);

	sort_array(test, SIZE);
	printf("Descending sorted provided array: \n");	
	print_array(test, SIZE);

	print_statistics(test, SIZE);

}

void print_statistics(unsigned char* numArray, unsigned int arrSize){
	
	int min = find_minimum(numArray, arrSize);
	int max = find_maximum(numArray, arrSize);
	int mean = find_mean(numArray, arrSize);
	int median = find_median(numArray, arrSize);

	printf("Minimum val in array: %d\n"
	      "Maximum val in array: %d\n"
              "Mean val of array: %d\n"	
              "Median val in array: %d\n", min, max, mean, median);

	printf("\n\n\n");


}

void print_array(unsigned char* numArray, unsigned int arrSize){
	
	unsigned char arrVal[20] = "";
	int numRows = arrSize/PRINT_ROW_SIZE + 1;
	int idx = 0;

	
	for(int row=0; row<numRows ; row++){
		for(int col=0; col < PRINT_ROW_SIZE && idx < arrSize; col++){
			sprintf(arrVal,"arr[%u]=%u", idx, numArray[idx]);
			printf("%15s",arrVal);
			idx++;
			
		}
		putchar('\n');
		if(idx == arrSize)
			break;		

	}

	printf("\n\n\n");

}

unsigned char find_median(unsigned char* numArray, unsigned int arrSize){

	if(arrSize == 0)
		return 0;

	sort_array(numArray, arrSize);
	if(arrSize % 2 > 0){ /*odd # elements in set - get middle index value of sorted array*/
		return numArray[arrSize/2];	
	}
	else{                /* even # elements in set - average the two middle values*/
		if(arrSize > 1)
			return (numArray[arrSize/2] + numArray[arrSize/2 - 1]) / 2;
		else
			return 0;
	}

}

unsigned char find_mean(unsigned char* numArray, unsigned int arrSize){
	
	unsigned int sum = 0;	
	
	if(arrSize == 0)
		return 0;

	for(int i=0; i<arrSize ; i++){	
		sum += numArray[i];
	}
	
	return (unsigned char)(sum / arrSize);
}

unsigned char find_maximum(unsigned char* numArray, unsigned int arrSize){
	
	unsigned char max = 0;	
	
	if(arrSize == 0)
		return 0;

	for(int i=0; i<arrSize ; i++){
		if(numArray[i] >= max)
			max = numArray[i];
	}
	
	return max;
}

unsigned char find_minimum(unsigned char* numArray, unsigned int arrSize){
	
	unsigned char min = 255;	
	
	if(arrSize == 0)
		return 0;

	for(int i=0; i<arrSize ; i++){
		if(numArray[i] <= min)
			min = numArray[i];
	}
	
	return min;
}

void sort_array(unsigned char* numArray, unsigned int arrSize){
	
	qsort(numArray, arrSize, sizeof(numArray[0]), sort_uchar_descending);
}

/*Private function passed as function pointer into qsort*/
int sort_uchar_descending(const void* ch1, const void* ch2){
	return *((unsigned char*)ch2) - *((unsigned char*)ch1);
}
