/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h
 * @brief Exposes public functions in stats.c that perform basic statistical functions on an array of unsigned chars
 *
 * Exposes public functions that perform basic functions on arrays of unsigned chars (0 - 255.)  Including:
 *		Printing stats, printing the array, returning array median,
 * 		returning array mean, returning array max, returning array min,
 * 		and sorting the array in place in descending order.
 *
 * @author Mark Jones
 * @date July 5th, 2018
 *
 */
#ifndef __STATS_H__
#define __STATS_H__


/**
 * @brief Prints some basic statistics on a provide array of small numbers
 *
 * A function that prints the statistics of a sorted or unsorted unsigned char array including its min, max, mean, and median
 *
 * @param numArray An array of numbers, sorted or unsorted, that contains <arrSize> number of small values between 0 and 255 
 * @param arrSize The number of elements in the aforementioned numArray
 *
 * @return Returns nothing
 */
void print_statistics(unsigned char* numArray, unsigned int arrSize);


/**
 * @brief Given an array of data and a length, prints the array to the screen
 *
 * Accepts a sorted or unsorted array of unsigned chars then prints it to the screen
 *
 * @param numArray An array of numbers, sorted or unsorted, that contains <arrSize> number of small values between 0 and 255 
 * @param arrSize The number of elements in the aforementioned numArray
 *
 * @return Returns nothing
 */
void print_array(unsigned char* numArray, unsigned int arrSize);


/**
 * @brief Given an array of data and a length, returns the median value
 *
 * Accepts a sorted or unsorted array of unsigned chars with its length and returns its median
 *
 * @param numArray An array of numbers, sorted or unsorted, that contains <arrSize> number of small values between 0 and 255
 * @param arrSize The number of elements in the aforementioned numArray
 *
 * @return Retuns numArray's median
 */
unsigned char find_median(unsigned char* numArray, unsigned int arrSize);


/**
 * @brief Given an array of data and a length, returns the mean
 *
 * Accepts a sorted or unsorted array of unsigned chars with its length and returns its mean
 *
 * @param numArray An array of numbers, sorted or unsorted, that contains <arrSize> number of small values between 0 and 255 
 * @param arrSize The number of elements in the aforementioned numArray
 *
 * @return Returns numArray's mean
 */
unsigned char find_mean(unsigned char* numArray, unsigned int arrSize);


/**
 * @brief Given an array of data and a length, returns the maximum
 *
 * Accepts an array of unsigned chars with its length and returns its maximum value
 *
 * @param numArray An array of numbers, sorted or unsorted, that contains <arrSize> number of small values between 0 and 255 
 * @param arrSize The number of elements in the aforementioned numArray
 *
 * @return Returns numArray's highest value
 */
unsigned char find_maximum(unsigned char* numArray, unsigned int arrSize);


/**
 * @brief Given an array of data and a length, returns the minimum
 *
 * Accepts a sorted or unsorted array of unsigned chars with its length and returns its minimum value
 *
 * @param numArray An array of numbers, sorted or unsorted, that contains <arrSize> number of small values between 0 and 255 
 * @param arrSize The number of elements in the aforementioned numArray
 *
 * @return Returns numArray's lowest value
 */
unsigned char find_minimum(unsigned char* numArray, unsigned int arrSize);


/**
 * @brief Given an array of data and a length, sorts the array from largest to smallest.
 *
 * Sorts a provided array of unsigned char values in place in descending order
 *
 * @param numArray An array of numbers, sorted or unsorted, that contains <arrSize> number of small values between 0 and 255 
 * @param arrSize The number of elements in the aforementioned numArray
 *
 * @return Returns nothing - numArray was sorted in place
 */
void sort_array(unsigned char* numArray, unsigned int arrSize);


#endif /* __STATS_H__ */
